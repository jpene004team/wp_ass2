#!/bin/bash
# the following code is modified from https://peteris.rocks/blog/unattended-installation-of-wordpress-on-ubuntu-server/

#update before installing software
sudo apt-get update
sudo apt-get install curl

#set up variables to be used during installation
WP_DOMAIN="$(curl ifconfig.me)"
WP_ADMIN_USERNAME="admin"
WP_ADMIN_PASSWORD="admin"
WP_ADMIN_EMAIL="no@spam.org"
WP_DB_NAME="wordpress"
WP_DB_USERNAME="wordpress"
WP_DB_PASSWORD="wordpress"
WP_PATH="/var/www/wordpress"
MYSQL_ROOT_PASSWORD="root"

#automates the root password prompt in the myslq-server
echo "mysql-server-5.7 mysql-server/root_password password $MYSQL_ROOT_PASSWORD" | sudo debconf-set-selections
echo "mysql-server-5.7 mysql-server/root_password_again password $MYSQL_ROOT_PASSWORD" | sudo debconf-set-selections

#install nginx and php-fpm first before php and mysql
sudo apt install -y nginx php-fpm  
sudo apt install -y php php-mysql php-curl php-gd mysql-server

wp_rest.sh